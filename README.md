## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/burdock/v/stable)](https://packagist.org/packages/shadoll/burdock)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/burdock/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/burdock/commits/master)
[![coverage report](https://gitlab.com/fabrika-klientov/libraries/burdock/badges/master/coverage.svg)](https://gitlab.com/fabrika-klientov/libraries/burdock/commits/master)
[![License](https://poser.pugx.org/shadoll/burdock/license)](https://packagist.org/packages/shadoll/burdock)

Telephony PHP API/hook library

---


## Install

`composer require shadoll/burdock`

---
