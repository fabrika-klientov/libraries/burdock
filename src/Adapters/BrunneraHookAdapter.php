<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.12
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Adapters;


use Brunnera\Adapters\HookAdapter;
use Brunnera\Client;
use Burdock\Adapters\Helpers\Normalize;
use Burdock\Contracts\BeHookAdapter;
use Burdock\Contracts\BePrimary;

class BrunneraHookAdapter extends HookAdapter implements BePrimary, BeHookAdapter
{
    use Normalize;

    /**
     * @param array $data
     * @param array $authData
     * @return void
     * */
    public function __construct(array $data = [], array $authData = null)
    {
        $this->authData = $authData;
        parent::__construct($data, empty($this->authData) ? null : new Client($this->authData));
    }

    /**
     * @return string
     */
    public function getSystem()
    {
        return 'phonet';
    }

    /**
     * @param array $data
     * @return bool
     * */
    public static function primary(array $data): bool
    {
        return isset($data['event']) || isset($data['callParentUuid']);
    }

}