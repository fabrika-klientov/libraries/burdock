<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.12
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Adapters;


use Burdock\Adapters\Helpers\Normalize;
use Burdock\Contracts\BeHookAdapter;
use Burdock\Contracts\BePrimary;
use Dianthus\Adapters\HookAdapter;
use Dianthus\Client;

class DianthusHookAdapter extends HookAdapter implements BePrimary, BeHookAdapter
{
    use Normalize;

    /**
     * @param array $data
     * @param array $authData
     * @return void
     * */
    public function __construct(array $data = [], array $authData = null)
    {
        $this->authData = $authData;
        parent::__construct($data, empty($this->authData) ? null : new Client($this->authData));
    }

    /**
     * @return string
     */
    public function getSystem()
    {
        return 'binotel';
    }

    /**
     * @param array $data
     * @return bool
     * */
    public static function primary(array $data): bool
    {
        return isset($data['requestType']) && (isset($data['companyID']) || isset($data['callDetails']['companyID']));
    }
}