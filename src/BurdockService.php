<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Burdock
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock;


use Burdock\Contracts\BeHookAdapter;
use Burdock\Helpers\ActionsAdapter;

class BurdockService
{
    use ActionsAdapter;

    /**
     * @var array $authData
     * */
    protected $authData;

    /**
     * @var static $service
     * */
    protected static $service;

    /**
     * @return void
     * */
    public function __construct()
    {
        self::$service = $this;
    }

    /** inject auth data
     * @param array $auth
     * @return $this
     * */
    public function setAuth(array $auth)
    {
        $this->authData = $auth;
        return $this;
    }

    /** run hook adapters
     * @param array $data
     * @param array $auth
     * @return BeHookAdapter|null
     * */
    public function run(array $data, array $auth = null)
    {
        $class = $this->getClassOfAdapter($data);
        if (isset($class)) {
            return new $class($data, $auth ?? $this->authData ?? null);
        }

        return null;
    }

    /** run hook adapters
     * @param array $data
     * @return string|null
     * */
    public function getClassOfAdapter(array $data)
    {
        foreach ($this->supportedAdapters as $adapter) {
            if ($adapter::primary($data)) {
                return $adapter;
            }
        }

        return null;
    }


    /**
     * @return static|null
     * */
    public static function context()
    {
        return self::$service ?? null;
    }

}