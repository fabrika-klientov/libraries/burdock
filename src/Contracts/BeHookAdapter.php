<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Contracts
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.12
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Contracts;


interface BeHookAdapter
{

    /**
     * @param array $data
     * @param array $authData
     * @return void
     * */
    public function __construct(array $data = [], array $authData = null);

    /**
     * @return string
     */
    public function getSystem();

    /**
     * @return string
     * */
    public function getRequestType();

    /**
     * @return void
     * @throws \Exception
     */
    public function validate();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return boolean
     */
    public function isIncoming();

    /**
     * @return boolean
     */
    public function isAnswered();

    /**
     * @return boolean
     */
    public function isCompleted();

    /**
     * @return boolean
     */
    public function isCallTracking();

    /**
     * @return boolean
     */
    public function isGetCall();

    /**
     * @param string|null $key
     * @return array|null
     */
    public function getTrackingData(string $key = null);

    /**
     * @param string|null $key
     * @return array|null
     */
    public function getCallData(string $key = null);

    /**
     * @param bool $normalize
     * @return mixed
     */
    public function getExternalNumber($normalize = false);

    /**
     * @param bool $normalize
     * @return mixed
     */
    public function getInternalNumber($normalize = false);

    /**
     * @param bool $normalize
     * @return mixed
     */
    public function getSourceNumber($normalize = false);

    /**
     * @return string|null
     */
    public function getSourceName();

    /**
     * @return string
     */
    public function getDisplayName();

    /**
     * @return string
     */
    public function getDisposition();

    /**
     * @return mixed
     */
    public function getSite();

    /**
     * @return string
     */
    public function getCompanyId();

    /**
     * @return array
     */
    public function getTags();

    /**
     * @return mixed
     */
    public function getStartTime();

    /**
     * @return mixed
     */
    public function getDuration();

    /**
     * @return string
     */
    public function getRecordLink();
}