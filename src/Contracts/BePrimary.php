<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Contracts
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Contracts;


interface BePrimary
{
    /** is active adapter
     * @param array $data
     * @return bool
     * */
    public static function primary(array $data): bool;
}