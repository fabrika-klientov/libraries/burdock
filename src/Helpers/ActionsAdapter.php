<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Helpers
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Helpers;


use Burdock\Contracts\BeHookAdapter;

trait ActionsAdapter
{
    /**
     * @var array $supportedAdapters of adapters
     * */
    protected $supportedAdapters = [];

    /**
     * @param array $collect ['\Burdock\Adapters\DianthusHookAdapter', BrunneraHookAdapter::class]
     * @return void
     * @throws \Exception
     * */
    public function initAdapters(array $collect)
    {
        foreach ($collect as $item) {
            $this->validateAdapter($item);
        }
        $this->supportedAdapters = $collect;
    }

    /**
     * @param string $class
     * @return $this
     * @throws \Exception
     * */
    public function pushAdapter(string $class)
    {
        $this->validateAdapter($class);
        $this->supportedAdapters[] = $class;

        return $this;
    }

    /**
     * @param string $class
     * @return $this
     * @throws \Exception
     * */
    public function removeAdapter(string $class)
    {
        $index = (int)array_search($class, $this->supportedAdapters);
        if ($index !== false) {
            array_splice($this->supportedAdapters, $index, 1);
        }

        return $this;
    }

    /**
     * @param string $class
     * @return void
     * @throws \Exception
     * */
    protected function validateAdapter(string $class)
    {
        if (class_exists($class) && is_subclass_of($class, BeHookAdapter::class)) {
            return;
        }

        throw new \Exception('Class [' . $class . '] not valid. Should implements [BeHookAdapter]');
    }
}