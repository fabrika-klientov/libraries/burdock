<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lara
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Lara\Middleware;

use Burdock\BurdockService;
use Closure;
use Lantana\Extensions\Guard\GuardService;

abstract class BaseAuthBurdock
{
    /**
     * @var BurdockService $burdockService
     */
    protected $burdockService;
    /**
     * @var GuardService $guardService
     */
    protected $guardService;

    /**
     * Create a new middleware instance.
     *
     * @param  BurdockService $burdockService
     * @param GuardService $guardService
     * @return void
     */
    public function __construct(BurdockService $burdockService, GuardService $guardService)
    {
        $this->burdockService = $burdockService;
        $this->guardService = $guardService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $this->init($request);

        return $next($request);
    }

    /**
     * @param \Illuminate\Http\Request  $request
     * @return bool
     * */
    abstract protected function init($request);
}