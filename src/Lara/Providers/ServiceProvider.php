<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lara
 * @category  Burdock
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Burdock\Lara\Providers;

use Burdock\BurdockService;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BurdockService::class, function ($app) {
            return new BurdockService();
        });
    }
}
