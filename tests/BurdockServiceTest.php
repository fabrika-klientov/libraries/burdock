<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 19:52
 */

namespace Tests;

use Burdock\BurdockService;
use PHPUnit\Framework\TestCase;

class BurdockServiceTest extends TestCase
{

    /**
     * @var BurdockService $service
     * */
    protected $service;
    protected $hookBinotel = '{"hash":"205766926e923bb8f2ae6fbcfdcbbce3200663c6","requestType":"apiCallCompleted","attemptsCounter":"1","callDetails":{"companyID":"26446","generalCallID":"1626082880","callID":"1626082883","startTime":"1575897836","callType":"1","internalNumber":"908","internalAdditionalData":"","externalNumber":"0509406186","waitsec":"12","billsec":"41","disposition":"ANSWER","isNewCall":"0","customerData":{"id":"29948973","name":"Олабин Олексій СОЛАНА ЛОГІСТИКА ТОВ"},"employeeData":{"name":"Луц Алеся Пожарка","email":"luts.alesya@euroservis.com.ua"},"pbxNumberData":{"number":"0503389676"},"historyData":[{"waitsec":"12","billsec":"41","disposition":"ANSWER","pbxNumberData":{"number":"0503389676"}}],"linkToCallRecordOverlayInMyBusiness":"https://my.binotel.ua/?module=history&subject=0509406186&sacte=ovl-link-pb-1626082880","linkToCallRecordInMyBusiness":"https://my.binotel.ua/?module=cdrs&action=generateFile&fileName=1626082883.mp3&callDate=2019-09-12_15:23&customerNumber=0509406186&callType=outgoing"}}';

    public function test__construct()
    {
        $this->assertInstanceOf(BurdockService::class, new BurdockService());
    }

    public function testSetAuth()
    {
        $this->assertInstanceOf(BurdockService::class, $this->service->setAuth(['any']));
    }

    public function testRun()
    {
        $this->service->initAdapters([
            \Burdock\Adapters\DianthusHookAdapter::class,
            \Burdock\Adapters\BrunneraHookAdapter::class,
        ]);

        $adapter = $this->service->run(json_decode($this->hookBinotel, true));

        $this->assertEquals('binotel', $adapter->getSystem());

        $this->expectException(\Exception::class);
        $this->service->initAdapters([
            \Burdock\Adapters\DianthusHookAdapter::class,
            \Brunnera\Adapters\HookAdapter::class,
        ]);
    }

    public function testContext()
    {
        $this->assertInstanceOf(BurdockService::class, BurdockService::context());
    }

    protected function setUp(): void
    {
        $this->service = new BurdockService();
    }

    protected function tearDown(): void
    {
        $this->service = null;
    }
}
